﻿Dim xl_path , xl_path_res  ,x_path_Res_TD
Dim r_tc , r_ts , tcExecute , tcID , sheetName , tsID
Dim vKeyword , vIP1 , vIP2 , pass, fail

' กำหนดที่อยู่ไฟล์ excel ของข้อมูลสำหรับการทดสอบ
xl_path =  "C:\Users\TheExiled\Desktop\QTP Book Online\QtpBookOnline.xlsx"
' กำหนดที่อยู่ไฟล์ excel สำหรับผลลัพธ์การทดสอบ
xl_path_res = "C:\Users\TheExiled\Desktop\QTP Book Online\BookOnlineResult\TestResult_"

' กำหนดที่อยู่รูปภาพการทำงานที่ผิดพลาด
environment("err_image") =  "C:\Users\TheExiled\Desktop\QTP Book Online\ErrorImages\Err_"

' สร้าง datatable  รอรับค่า
datatable.AddSheet "dTC" ' Test Cases
datatable.AddSheet "dTS" ' Test Steps

' Import ข้อมูลจากไฟล์ excel ลงใน datatable
datatable.ImportSheet xl_path, "TestCases", "dTC"
datatable.ImportSheet xl_path, "TestSteps", "dTS"

' อ่านค่าจำนวนแถวจาก Datatable
r_tc = datatable.GetSheet("dTC").GetRowCount() 
r_ts = datatable.GetSheet("dTS").GetRowCount() 

For i = 1 To r_tc Step 1
	datatable.Value("Result","dTC") = ""
	datatable.Value("Pass","dTC") = ""
	datatable.Value("Fail","dTC") = ""
	datatable.GetSheet("dTC").SetCurrentRow(i)
	tcExecute = datatable.Value("Execute","dTC")
	tcID = datatable.Value("TCID","dTC")
	' ตรวจสอบว่าต้องการทำการทดสอบ test case นี้หรือไม่
	If UCase(tcExecute) = "Y" Then
		' นำข้อมูลสำหรับการทดสอบทั้งหมดมาเก็บไฟล์ที่ datatable
		sheetName = datatable.Value("Name","dTC")
		datatable.AddSheet "d"&sheetName
		datatable.ImportSheet xl_path, sheetName, "d"&sheetName
		r_sheet = datatable.GetSheet("d"&sheetName).GetRowCount()
		pass = 0
		fail = 0
		For j = 1 To r_sheet Step 1
			datatable.GetSheet("d"&sheetName).SetCurrentRow(j)
			' กำหนดสถานะการทำงานเป็น 0 หากมีการทำงานผิดพลาดจะเปลี่ยนค่าภายในตัวแปรนี้
			status = 0	
			For k = 1 To r_ts Step 1
				' นำข้อมูลในการทดสอบในแถว มาเก็บในตัวแปรประเภท environment
				datatable.GetSheet("dTS").SetCurrentRow(k)
				tsID = datatable.Value("TSID","dTS")
				get_Test_Data sheetName	
					
				If tcID = tsID Then
					' เรียกใช้งานคีย์เวิร์ดฟังก์ชัน
					vKeyword = datatable.Value("Keyword","dTS")
					vIP1 = datatable.Value("IP1","dTS")
					vIP2 = datatable.Value("IP2","dTS")
					environment("vKeyword") = vKeyword
					kw_executor vIP1, vIP2
					
					
					If environment("Result") = "Fail" Then
						' จัดการกับข้อผิดพลาดที่เกิดขึ้น
						manage_err
                        ' บันทึกสาเหตุการเกิดข้อผิดพลาดจากลงในคอลัมน์ Error
                        datatable.Value("Result","d"&sheetName) = "Fail"
                        datatable.Value("Error","d"&sheetName) = environment ("Error")
                        ' นับจำนวนครั้งที่เกิดข้อผิดพลาดระหว่างการทดสอบ
                        fail = fail + 1
                        ' จบการทดสอบของข้อมูลแถวนั้น
						Exit For 
					Else  
						datatable.Value("Result","d"&sheetName) = "Pass"
						datatable.Value("Error","d"&sheetName) = environment ("Error")
					End If
					status = 1
				 ElseIf status = 1 Then
				 	' นับจำนวนครั้งเมื่อทดสอบสำเร็จ
				 	pass = pass + 1
	   				Exit For 

				End If
				
			
			Next
			' คำนวนและบันทึกผลลัพธ์การทดสอบ
			datatable.Value("Pass","dTC") = ((pass*100)\r_sheet)&"%"
			datatable.Value("Fail","dTC") = ((fail*100)\r_sheet)&"%"
			' หากผลเกิดข้อผิดพลาดมากกว่า 50 % ผลการทดสอบของ test case จะเป็น fail
			If ((fail*100)\r_sheet) > 50 Then
				datatable.Value("Result","dTC") = "Fail"
			Else 
				datatable.Value("Result","dTC") = "Pass"
			End If
			' สร้างไฟล์ excel จาก datatable
			x_path_Res_TD =  xl_path_res &sheetName & ".xls"
			datatable.ExportSheet  x_path_res_TD,"dTS"
			datatable.ExportSheet  x_path_res_TD,"dTC"
			datatable.ExportSheet  x_path_res_TD,"d"&sheetName
		Next
	End If
	
	
Next
